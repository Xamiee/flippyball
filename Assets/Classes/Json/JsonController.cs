﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class JsonController : MonoBehaviour
{
    private Game_Manager _gameManager;
    private Game_Data _gameData;
    private string _fileName;
    private string _filePath;
    private string _saveDirectory;


    public void Initialize(Game_Manager gameManager)
    {
        _fileName = "save.json";

        #if UNITY_ANDROID
                _saveDirectory = Application.persistentDataPath + "/Saves/";
        #endif

        #if UNITY_EDITOR
            _saveDirectory = Application.dataPath + "/Saves/";
        #endif

        _gameManager = gameManager;
        _gameData = new Game_Data();

        if (!Directory.Exists(_saveDirectory))
        {
            Directory.CreateDirectory(_saveDirectory);
        }

        _filePath = Path.Combine(_saveDirectory, _fileName);

        Load();

    }

    public void Save()
    {
        string json = JsonUtility.ToJson(_gameData);

        if (!File.Exists(_filePath))
        {
            File.Create(_filePath).Dispose();
        }

        File.WriteAllText(_filePath, json);
    }

    public void Load()
    {
        string json;
        if (!File.Exists(_filePath))
        {
            SetDefaults_GameData();
            Save();
        }

        if (File.Exists(_filePath))
        {
            json = File.ReadAllText(_filePath);
            _gameData = JsonUtility.FromJson<Game_Data>(json);
        }

    }


    public void SetDefaults_GameData()
    {
        _gameData.HighScore = 0;
    }

    public object GetValue(GameData_Type _gamedataType)
    {
        return _gameData.HighScore;
    }
    public void SetValue(GameData_Type _gameDataType,object value)
    {
        switch(_gameDataType)
        {
            case GameData_Type.Highscore:
                _gameData.HighScore = (int) value;
                break;
            default:
                Debug.Log("ERROR : SET VALUE JSON - " + _gameDataType);
                break;
        }

        Save();
    }
}
