﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gameplay_GameoverChecker : MonoBehaviour
{
    private Gameplay_Controller _gameplayController;

    public void Initialize(Gameplay_Controller gameplayController)
    {
        _gameplayController = gameplayController;
    }
    public void CheckIfGameOver()
    {
        int healthLeft = (int)_gameplayController._gameManager._playerController.GetStatus(StatusType.Health);
        if (healthLeft == 0)
        {
            _gameplayController._gameManager.GameOver();
        }

    }
}
