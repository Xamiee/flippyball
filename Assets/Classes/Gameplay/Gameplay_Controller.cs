﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gameplay_Controller : MonoBehaviour
{
    public Game_Manager _gameManager;
    [SerializeField] public Gameplay_Pool _gameplayPool;
    [SerializeField] public Gameplay_SpawnObjects _gameplaySpawnObjects;
    [SerializeField] public Gameplay_GameoverChecker _gameplayGameoverChecker;
    [SerializeField] public Gameplay_DeathzoneTrigger _gameplayDeathZoneTrigger;

    public void Initialize(Game_Manager gameManager)
    {
        _gameManager = gameManager;
        _gameplayGameoverChecker.Initialize(this);
        _gameplaySpawnObjects.Initialize(this);
        _gameplayPool.Initialize(this);
        _gameplayDeathZoneTrigger.Initialize(this); 

    }

    
    public void GameOver()
    {
        _gameplaySpawnObjects.GameOver();
        _gameplayPool.GameOver();
        
    }
       
    public void NewGame()
    {
        _gameplaySpawnObjects.NewGame();
    }
}
