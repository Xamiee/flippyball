﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball_Restart : MonoBehaviour
{
    private void OnEnable()
    {
        Restart();
    }

    void Restart()
    {
        transform.position = new Vector3(Random.Range(-2, 2), 3.9f, transform.position.z);
    }
}
