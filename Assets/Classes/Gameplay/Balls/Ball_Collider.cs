﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball_Collider : MonoBehaviour, IOnHitBall
{
    [SerializeField] Rigidbody2D _rigidbody2D;
    [SerializeField] private int hitValue;

   
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag=="Player")
        {
            Vector3 direction = (transform.position - collision.transform.position).normalized;
           
            float force = 10f;
            if(direction.y<0.6f)
            {
                direction = new Vector3(direction.x, 0.8f, direction.z);
                force = 12f;
            }
            if (direction.x > 0.4f)
            {
                direction = new Vector3(0.4f, direction.y, direction.z);
                force = 12f;
            }
            if (direction.x < -0.4f)
            {
                direction = new Vector3(-0.4f, direction.y, direction.z);
                force = 12f;
            }
            Vector2 point = collision.GetContact(0).point;
            Addforce(direction, force, point);
        }
      
    }


    void Addforce(Vector2 direction,float force, Vector2 point)
    {
        _rigidbody2D.velocity = Vector2.zero;
        //_rigidbody2D.AddForceAtPosition(direction * force, point,ForceMode2D.Impulse);
        _rigidbody2D.AddForce(direction * force, ForceMode2D.Impulse);
    }

    public int GetHitValue()
    {
        return hitValue;
    }
}
