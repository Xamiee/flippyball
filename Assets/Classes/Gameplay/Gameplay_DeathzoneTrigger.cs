﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gameplay_DeathzoneTrigger : MonoBehaviour
{
    private Gameplay_Controller _gameplayController;
    public void Initialize(Gameplay_Controller gameplayController)
    {
        _gameplayController = gameplayController;

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Ball")
        {
            collision.gameObject.SetActive(false);
            _gameplayController._gameManager._playerController.DealDamage(1);
            

        }
    }
}
