﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gameplay_SpawnObjects : MonoBehaviour
{
    private Gameplay_Controller _gameplayController;
    
    public void Initialize(Gameplay_Controller gameplayController)
    {
        _gameplayController = gameplayController;
        Debug.Log("START REPEATING");
        InvokeRepeating("Spawn", 5f, 2f);
    }
    
    
    public void StopSpawning()
    {
            CancelInvoke();
    }
    public void StartSpawning()
    {
        InvokeRepeating("Spawn", 2f, 3f);
    }
    private void Spawn()
    {
        Debug.Log("SPAWNING");
        if(_gameplayController._gameplayPool.GetNumberOfInactiveBalls()>0)
        {
            _gameplayController._gameplayPool.GetInactiveBall().SetActive(true);
        }
    }

    public void GameOver()
    {
        StopSpawning();
    }

    public void NewGame()
    {
        StartSpawning();
    }
}
