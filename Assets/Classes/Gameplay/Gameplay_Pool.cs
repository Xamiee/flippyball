﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gameplay_Pool : MonoBehaviour
{
    [HideInInspector] public List<GameObject> PooledObjects;
    [SerializeField] private GameObject _ballPrefab;
    [SerializeField] private int _amountToPool = 5;
    private Gameplay_Controller _gameplayController;

    public void Initialize(Gameplay_Controller gameplayController)
    {
        _gameplayController = gameplayController;
        CreatePool();
    }
    public void CreatePool()
    {
        for (int i = 0; i < _amountToPool; i++)
        {
            GameObject obj = (GameObject)Instantiate(_ballPrefab);
            //obj.transform.parent = transform.parent;
            obj.SetActive(false);
            PooledObjects.Add(obj);
        }
    }

    public GameObject GetInactiveBall()
    {
        for (int i = 0; i < PooledObjects.Count; i++)
        {
            if (!PooledObjects[i].activeInHierarchy)
            {
                return PooledObjects[i];
            }
        }
        return null;
    }

    public int GetNumberOfInactiveBalls()
    {
        int number=0;
        for (int i = 0; i < PooledObjects.Count; i++)
        {
            if (!PooledObjects[i].activeInHierarchy)
            {
                number++;
            }
        }
        return number;
    }
    public void DisableAllBalls()
    {
        for (int i = 0; i < PooledObjects.Count; i++)
        {
            if (PooledObjects[i].activeInHierarchy)
            {
                PooledObjects[i].SetActive(false);
            }
        }
    }
    public void GameOver()
    {
        DisableAllBalls();
    }
}
