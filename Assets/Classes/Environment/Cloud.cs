﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cloud : MonoBehaviour
{
    Vector3 _EarlyPosition;
    bool canDisable = false;
    bool canMove = false;

    [SerializeField] Clouds_MovementDirection _movementDirection;
    [SerializeField] float _movementSpeed;

    private void Awake()
    {
        _EarlyPosition = transform.position;
        Object_Reset();
    }
    
    public void Object_Reset()
    {

        canMove = true;
            canDisable = false;
            transform.position = _EarlyPosition;

    }
    private void FixedUpdate()
    {
        if(canMove)
        CloudMovement(_movementDirection, _movementSpeed);
        
    }
    

    private void OnBecameVisible()
    {
        canDisable = true;
    }
    private void OnBecameInvisible()
    {
        if (canDisable)
            Object_Reset();
    }

    


    void CloudMovement(Clouds_MovementDirection movementDirection, float movementSpeed)
    {
        Vector3 direction;

        if (movementDirection == Clouds_MovementDirection.right)
            direction = Vector3.right;
        else if (movementDirection == Clouds_MovementDirection.left)
            direction = Vector3.left;
        else
            direction = new Vector3(0, 0, 0);

        transform.Translate(direction * movementSpeed * Time.deltaTime);
    }
}
