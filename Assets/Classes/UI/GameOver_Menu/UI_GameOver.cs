﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class UI_GameOver:MonoBehaviour
{
    [SerializeField] UI_Controller _uiController;
    [SerializeField] TextMeshProUGUI _endingScore;
    [SerializeField] GameObject _gameoverMainObject;
    [SerializeField] Button _restartButton;

    public void Initialize(UI_Controller uiController)
    {
        _restartButton.onClick.AddListener(() => Restart());
        _uiController = uiController;

    }
    private void Restart()
    {
        _uiController._gameManager.NewGame();
    }

    public void OpenWindow()
    {
        _gameoverMainObject.SetActive(true);
        UpdateValues();
    }
    
    public void CloseWindow()
    {
        _gameoverMainObject.SetActive(false);
    }
    public void UpdateValues()
    {
        int temp_currentScore =(int) _uiController._gameManager._scoreController.GetScore(Score_Type.CurrentScore);
        _endingScore.text = "Twój wynik wynosi "+ temp_currentScore + " podbić.";

    }

    public void GameOver()
    {
        OpenWindow();
    }
    public void NewGame()
    {
        CloseWindow();
    }
}
