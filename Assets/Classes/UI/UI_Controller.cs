﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_Controller : MonoBehaviour
{
    public Game_Manager _gameManager;
    [SerializeField] UI_GameOver _uiGameOver;
    [SerializeField] UI_HealthBar _uiHealthBar;

    public void Initialize(Game_Manager gameManager)
    {
        _gameManager = gameManager;
        _uiGameOver.Initialize(this);
        _uiHealthBar.Initialize(this);
    }

    public void GameOver()
    {
        _uiGameOver.GameOver();
    }

    public void NewGame()
    {
        _uiGameOver.NewGame();
        UpdateHealthBar();
    }

    public void UpdateHealthBar()
    {
        _uiHealthBar.UpdateHealthBar();
    }
    
}
