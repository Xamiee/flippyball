﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_HealthBar : MonoBehaviour
{
    private UI_Controller _uiController;


    [SerializeField] GameObject health_1;
    [SerializeField] GameObject health_2;
    [SerializeField] GameObject health_3;


    public void Initialize(UI_Controller uiController)
    {
        _uiController = uiController;
    }
    public void UpdateHealthBar()
    {
        int currentHealth =(int) _uiController._gameManager._playerController.GetStatus(StatusType.Health);

        if(currentHealth==3)
        {
            health_1.SetActive(true);
            health_2.SetActive(true);
            health_3.SetActive(true);
        }
        else if(currentHealth==2)
        {
            health_1.SetActive(true);
            health_2.SetActive(true);
            health_3.SetActive(false);
        }
        else if (currentHealth == 1)
        {
            health_1.SetActive(true);
            health_2.SetActive(false);
            health_3.SetActive(false);
        }
        else 
        {
            health_1.SetActive(false);
            health_2.SetActive(false);
            health_3.SetActive(false);
        }
    }
}
