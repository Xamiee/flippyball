﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game_Manager : MonoBehaviour
{
    [SerializeField] public Player_Controller _playerController;
    [SerializeField] public Score_Controller _scoreController;
    [SerializeField] public JsonController _jsonController;
    [SerializeField] public Gameplay_Controller _gameplayController;
    [SerializeField] public UI_Controller _uiController;
    [SerializeField] public Game_Data _gameData;
    private void Awake()
    {
        // _playerController.Initialize(this);
        _uiController.Initialize(this);
        _jsonController.Initialize(this);
        _scoreController.Initialize(this);
        _playerController.Initialize(this);
        _gameplayController.Initialize(this);
    }

    public void GameOver()
    {
        _uiController.GameOver();
        _gameplayController.GameOver();
    }

    public void NewGame()
    {
        _scoreController.NewGame();
        _playerController.NewGame();
        _uiController.NewGame();
        _gameplayController.NewGame();
    }

    
}
