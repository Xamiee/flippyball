﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Score_Controller : MonoBehaviour
{
    #region Delegates


    public delegate void OnScore(Score_Type _scoreType, object value);
    public static event OnScore OnGettingScore;

    #endregion

    #region References and variables

    public Game_Manager _gameManager;


    [SerializeField] Score_Ui _scoreUi;
    [SerializeField] Score_Variables _scoreVariables;
    

    #endregion

    #region Initialize

    public void Initialize(Game_Manager gameManager)
    {
        _gameManager = gameManager;

        _scoreVariables.Initialize(this);
        _scoreUi.Initialize(this);
        OnGettingScore += SetScore;

        _scoreVariables.SetScore(Score_Type.Highscore, _gameManager._jsonController.GetValue(GameData_Type.Highscore));
        _scoreUi.UpdateUi();
    }

    #endregion

    #region Score_Ui
    public void UpdateUi()
    {
        _scoreUi.UpdateUi();
    }
    #endregion

    #region Variables

    public object GetScore(Score_Type _scoreType)
    {
       return _scoreVariables.Get_Score(_scoreType);
    }

    public void SetScore(Score_Type _scoreType, object value)
    {
        Debug.Log("SET SCORE: " + value +", ScoreType: " + _scoreType);
        _scoreVariables.SetScore(_scoreType, value);
        UpdateUi();
    }
    #endregion


    #region Delegate Method


    public void SendEvent_OnScore(Score_Type _scoreType, int value)
    {
        OnGettingScore(_scoreType, value);
    }


    #endregion

    #region Gamestate
    public void NewGame()
    {
        _scoreVariables.SetDefaults();
        UpdateUi();
    }
    
    #endregion
}
