﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class Score_Ui : MonoBehaviour
{
    private Score_Controller _scoreController;

    [SerializeField] TextMeshProUGUI text_CurrentScore;
    [SerializeField] TextMeshProUGUI text_HighScore;
    

    public void Initialize(Score_Controller scoreController)
    {
        _scoreController = scoreController;
        UpdateUi();
    }
    public void UpdateUi()
    {
        int temp_currentScore = (int) _scoreController.GetScore(Score_Type.CurrentScore);
        int temp_highscore = (int) _scoreController.GetScore(Score_Type.Highscore);


        text_CurrentScore.text = "Current Score: " + temp_currentScore;
        text_HighScore.text = "HighScore: " + temp_highscore;
    }
}
