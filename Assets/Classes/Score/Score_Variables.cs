﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Score_Variables : MonoBehaviour
{
    private int CurrentScore;
    private int Highscore;

    private Score_Controller _scoreController;


    public void Initialize(Score_Controller scoreController)
    {
        _scoreController = scoreController;

        LoadValues();
    }

    private void LoadValues()
    {
        Highscore = (int)_scoreController._gameManager._jsonController.GetValue(GameData_Type.Highscore);
    }
   
    private void SaveValues(GameData_Type _gameDataType)
    {
        switch (_gameDataType)
        {
            case GameData_Type.Highscore:
                _scoreController._gameManager._jsonController.SetValue(_gameDataType, Highscore);
                break;
            default:
                Debug.Log("ERROR: SaveValues : " + _gameDataType);
                break;
        }
    }

    public object Get_Score(Score_Type _scoreType)
    {
        switch(_scoreType)
        {
            case Score_Type.Highscore:
                return Highscore;
            case Score_Type.CurrentScore:
                return CurrentScore;
            default:
                Debug.Log("ERROR: GETSCORE : " + _scoreType);
                return null;
        }
    }

    public void SetScore(Score_Type _scoreType, object value)
    {
        switch (_scoreType)
        {
            case Score_Type.Highscore:
                Highscore = (int)value;
                break;
            case Score_Type.CurrentScore:
                CurrentScore += (int)value;
                break;
            default:
                Debug.Log("ERROR: SETSCORE : " + _scoreType);
                break;
        }

        AdditionalFunctions(_scoreType, value);
    }

    private void AdditionalFunctions(Score_Type _scoreType, object value)
    {
        switch (_scoreType)
        {
            case Score_Type.Highscore:
                break;
            case Score_Type.CurrentScore:
                Check_IsCurrentScoreHigher();
                break;
            default:
                Debug.Log("ERROR: ADDITIONAL FUNCTIONS : " + _scoreType);
                break;
        }
    }

    void Check_IsCurrentScoreHigher()
    {
        if(CurrentScore>Highscore)
        {
            Highscore = CurrentScore;
            Debug.Log("SaveValues");
            SaveValues(GameData_Type.Highscore);
        }

    }

    public void SetDefaults()
    {
        CurrentScore = 0;
    }
}
