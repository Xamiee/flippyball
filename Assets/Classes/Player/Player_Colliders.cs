﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Colliders : MonoBehaviour
{
    private Player_Controller _playerController;

    public void Initialize(Player_Controller playerController)
    {
        _playerController = playerController;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag=="Ball")
        {
            GettingScore(collision.gameObject.GetComponent<IOnHitBall>().GetHitValue());
        }

    }

    void GettingScore(int value)
    {
        _playerController.SendEvent_OnScore(Score_Type.CurrentScore, value);
    }
}
