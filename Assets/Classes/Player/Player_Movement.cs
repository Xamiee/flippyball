﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Movement : MonoBehaviour
{
    private Player_Controller _playerController;
    float tempSpeed = 0;

    public void Initiate(Player_Controller playerController)
    {
        _playerController = playerController;

        Debug.Log("_playerController: " + _playerController);
    }
   
    public void StartMoving(MovementDirection _movementDirection)
    {
        //Validate Speed statement
        //if(tempSpeed!= (float) _playerController.GetStatus(StatusType.MovementSpeed))
            tempSpeed = (float) _playerController.GetStatus(StatusType.MovementSpeed);

        if (_movementDirection == MovementDirection.right)
            Move_Right();
        else
            Move_Left();
    }

    
    void Move_Left()
    {
        gameObject.transform.Translate(new Vector3(tempSpeed, 0, 0) * Time.deltaTime);
    }
    void Move_Right()
    {
        gameObject.transform.Translate(new Vector3(tempSpeed, 0, 0) * Time.deltaTime);
    }
}
