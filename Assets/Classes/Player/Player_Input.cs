﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Player_Input : MonoBehaviour
{
    bool tempIsPressed = false;

    Player_Controller _playerController;

    [SerializeField] UIButton_Handler _buttonLeft;
    [SerializeField] UIButton_Handler _buttonRight;

    MovementDirection tempMovementDirection = MovementDirection.right;



    public void Initiate(Player_Controller playerController)
    {
        _playerController = playerController;
    }

    private void FixedUpdate()
    {
        MovementListener();

        VariableListener();
    }
   
    void MovementListener()
    {
        if (_buttonLeft.IsPressed)
            StartMoving(MovementDirection.left);

        if (_buttonRight.IsPressed)
            StartMoving(MovementDirection.right);

    }
    void StartMoving(MovementDirection _movementDirection)
    {
        _playerController.StartMoving(_movementDirection);

        if(_movementDirection!=tempMovementDirection)
        {
            _playerController.Transform_ChangeDirection(_movementDirection);
            tempMovementDirection = _movementDirection;
        }
    }
   
    void VariableListener()
    {
        Check_IsPressed();
    }


    void Check_IsPressed()
    {
        if (_buttonLeft.IsPressed || _buttonRight.IsPressed)
        {
            if (tempIsPressed == false)
            {
                _playerController.SetStatus(StatusType.IsPressed, true);
                tempIsPressed = true;
            }
        }
        else
        {
            if (tempIsPressed == true)
            {
                _playerController.SetStatus(StatusType.IsPressed, false);
                tempIsPressed = false;
            }
        }
    }
}
