﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Animation: MonoBehaviour
{
    [SerializeField] Animator player_animator;

    Player_Controller _playerController;

    public void Initiate(Player_Controller playerController)
    {
        _playerController = playerController;
    }

    public void UpdateParameter_BoolType(string parameterName, bool value)
    {
        player_animator.SetBool(parameterName, value);
        
    }
}
