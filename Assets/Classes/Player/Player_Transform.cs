﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Transform : MonoBehaviour
{
    

    public void ChangeDirection(MovementDirection _movementDirection)
    {
        if (_movementDirection == MovementDirection.left)
            transform.rotation = new Quaternion(0, 180, 0,0);

        if (_movementDirection == MovementDirection.right)
            transform.rotation = new Quaternion(0, 0, 0, 0);
    }



}
