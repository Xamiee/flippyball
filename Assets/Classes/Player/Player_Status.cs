﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Player_Status : MonoBehaviour
{
    public bool IsPressed;
    public float _MovementSpeed = 3f;
    public int healthLeft;

    public MovementDirection _movementDirection;



    public object GetStatus(StatusType statustype)
    {
        switch(statustype)
        {
            case StatusType.IsPressed:
                return IsPressed;
            case StatusType.MovementSpeed:
                return _MovementSpeed;
            case StatusType.MovementDirection:
                return _movementDirection;
            case StatusType.Health:
                return healthLeft;
            default:
                Debug.Log("ERROR, GETSTATUS - " + statustype);
                return null;
        }
    }

    public void SetStatus(StatusType statustype, object value)
    {
        switch (statustype)
        {
            case StatusType.IsPressed:
                IsPressed = (bool)value;
                break;
            case StatusType.MovementDirection:
                _movementDirection = (MovementDirection)value;
                break;
            case StatusType.MovementSpeed:
                _MovementSpeed = (float)value;
                break;
            case StatusType.Health:
                healthLeft -=(int) value;
                break;
            default:
                Debug.Log("ERROR, SETSTATUS - " + statustype);
                break;
        }
    }

    public void NewGame()
    {
        healthLeft = 3;
    }

}
