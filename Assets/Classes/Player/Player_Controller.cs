﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Controller : MonoBehaviour
{
    #region Variables && References
    [SerializeField] Game_Manager _gameManager;

    [SerializeField] Player_Input _playerInput;
    [SerializeField] Player_Movement _playerMovement;
    [SerializeField] Player_Status _playerStatus;
    [SerializeField] Player_Animation _playerAnimation;
    [SerializeField] Player_Transform _playerTransform;
    [SerializeField] Player_Colliders _playerColliders;

    #endregion

    #region Initialize

    public void Initialize(Game_Manager gameManager)
    {
        _gameManager = gameManager;

        _playerInput.Initiate(this);
        _playerMovement.Initiate(this);
        _playerColliders.Initialize(this);
    }
    #endregion

    #region Movement

    public void StartMoving(MovementDirection _movementDirection)
    {
        _playerMovement.StartMoving(_movementDirection);
    }


    #endregion

    #region Transform

    public void Transform_ChangeDirection(MovementDirection _movementDirection)
    {
        _playerTransform.ChangeDirection(_movementDirection);
    }
    #endregion

    #region Status
    public object GetStatus(StatusType stattype)
    {
        return _playerStatus.GetStatus(stattype);
    }
    public void SetStatus(StatusType stattype, object value)
    {
        _playerStatus.SetStatus(stattype, value);

        AdditionalAction(stattype,value);
    }



    private void AdditionalAction(StatusType stattype,object value)
    {
        switch (stattype)
        {
            case StatusType.IsPressed:
                _playerAnimation.UpdateParameter_BoolType("IsPressed", (bool)value);
                break;
            case StatusType.MovementDirection:
                break;
            case StatusType.MovementSpeed:
                break;
            case StatusType.Health:
                break;
        }
    }



    #endregion

    #region Colliders


    public void SendEvent_OnScore(Score_Type _scoreType, int value)
    {
        _gameManager._scoreController.SendEvent_OnScore(_scoreType, value);
    }
    #endregion


    #region Health

    public void DealDamage(int value)
    {
        SetStatus(StatusType.Health, value);
        _gameManager._gameplayController._gameplayGameoverChecker.CheckIfGameOver();
        _gameManager._uiController.UpdateHealthBar();
    }
    #endregion

    #region GameStatus
    public void NewGame()
    {
        _playerStatus.NewGame();
    }
    
    #endregion
}
